package ir.amirshahkarami.wedefeatcorona.utils;

import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

public class Util {
    public static void requestFullScreenActivity(AppCompatActivity activity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow()
                .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
//    public static void setupLanguageUtil(AppCompatActivity activity) {
//        switch (getCurrentLanguage()) {
//            case "en":
//                setApplicationLangApi28("en", activity);
//                break;
//            case "fa":
//                setApplicationLangApi28("fa", activity);
//                break;
//            case "ar":
//                setApplicationLangApi28("ar", activity);
//                break;
//        }
//
//    }
//    public static String getCurrentLanguage() {
//        return App.getApplicationSharedPreferences().getString("language", "fa");
//    }
}
