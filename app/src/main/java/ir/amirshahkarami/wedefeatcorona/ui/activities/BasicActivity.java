package ir.amirshahkarami.wedefeatcorona.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import ir.amirshahkarami.wedefeatcorona.R;

import static ir.amirshahkarami.wedefeatcorona.utils.Util.requestFullScreenActivity;

public class BasicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestFullScreenActivity(this);
        setContentView(R.layout.menu_activity_basic);
        LinearLayout notification_lnl = findViewById(R.id.notification_lnl);
        notification_lnl.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        });
    }
}
