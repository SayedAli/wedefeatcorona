package ir.amirshahkarami.wedefeatcorona.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ir.amirshahkarami.wedefeatcorona.R;

import static ir.amirshahkarami.wedefeatcorona.utils.Util.requestFullScreenActivity;

public class NotificationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestFullScreenActivity(this);
        setContentView(R.layout.activity_notification);

    }
}
