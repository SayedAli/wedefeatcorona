package ir.amirshahkarami.wedefeatcorona.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import ir.amirshahkarami.wedefeatcorona.R;
import ir.amirshahkarami.wedefeatcorona.utils.Util;

public class Splash extends AppCompatActivity {

    private boolean userE ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        Util.requestFullScreenActivity(this);
        setContentView(R.layout.splash);

//        userE = App.getApplicationSharedPreferences().getBoolean("userE", false);
//        if (!userE)
//            App.getApplicationShpEditor().putBoolean("isOnePaging",true).apply();
//        imageView.setAnimation(AnimationUtils.loadAnimation(this, R.anim.zoom_splash));
        new Handler().postDelayed(() -> {
//            if(App.getApplicationSharedPreferences().getBoolean("user_skipped", false) ||
//                    App.getApplicationSharedPreferences().getBoolean("user_registered", false)){
                startActivity(new Intent(getApplicationContext(), BasicActivity.class));
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//            }else{
//                startActivity(new Intent(getApplicationContext(), Register.class));
//                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//            }
            finish();
        }, 2500);
    }
    @Override
    protected void onDestroy(){
//        imageView.setImageDrawable(null);
        super.onDestroy();
    }
}
